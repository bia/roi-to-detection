package plugins.stef.tools;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Map;

import icy.gui.dialog.MessageDialog;
import icy.main.Icy;
import icy.roi.BooleanMask2D;
import icy.roi.ROI;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.swimmingPool.SwimmingObject;
import icy.swimmingPool.SwimmingPool;
import icy.type.point.Point5D;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.kernel.roi.descriptor.intensity.ROIIntensityDescriptorsPlugin;
import plugins.kernel.roi.descriptor.measure.ROIMassCenterDescriptorsPlugin;
import plugins.nchenouard.spot.DetectionResult;
import plugins.nchenouard.spot.Point3D;
import plugins.nchenouard.spot.Spot;

/**
 * This plugin / block allow to convert a set of ROI into a DetectionResult object containing a set of Detection.
 * 
 * @author Stephane
 */
public class ROIToDetection extends EzPlug implements Block
{
    protected final EzVarSequence varSequence;
    protected final VarROIArray varRois;
    protected final VarBoolean exportSP;
    protected final Var<DetectionResult> varResult;

    public ROIToDetection()
    {
        super();

        varSequence = new EzVarSequence("Sequence");
        varRois = new VarROIArray("ROI(s)");
        exportSP = new VarBoolean("Export to Swimming Pool", Boolean.TRUE);
        varResult = new Var<DetectionResult>("Detection result", new DetectionResult());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSequence.getVariable());
        inputMap.add("rois", varRois);
        inputMap.add("exportSP", exportSP);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("detection result", varResult);
    }

    @Override
    protected void initialize()
    {
        addEzComponent(varSequence);
    }

    @Override
    public void clean()
    {
        //
    }

    @Override
    protected void execute()
    {
        ROI[] rois;
        Sequence sequence = varSequence.getValue();

        // headless / protocol mode --> get ROI from ROIS variable
        if (isHeadLess())
            rois = varRois.getValue();
        // interactive mode
        else
        {
            if (sequence != null)
            {
                // then try to get it from Sequence
                rois = sequence.getROIs().toArray(new ROI[0]);

                if (rois.length == 0)
                {
                    MessageDialog.showDialog("The selected Sequence doesn't contain any ROI",
                            MessageDialog.INFORMATION_MESSAGE);
                }
            }
            else
                rois = null;
        }

        DetectionResult detectionResult = new DetectionResult();

        // something to do ?
        if ((rois != null) && (rois.length > 0))
        {
            try
            {
                detectionResult = convertToDetectionResult(rois, sequence);
            }
            catch (InterruptedException e)
            {
                if (!isHeadLess())
                    MessageDialog.showDialog("ROI to Detection process has been interrupted !");

                varResult.setValue(null);

                return;
            }
        }

        // set result
        varResult.setValue(detectionResult);

        // export asked ?
        if (exportSP.getValue().booleanValue())
        {
            // export detection result object in the swimming pool
            final SwimmingPool sp = Icy.getMainInterface().getSwimmingPool();
            if (sp != null)
            {
                final SwimmingObject obj = new SwimmingObject(detectionResult, "Detection results");
                sp.add(obj);
            }
        }
    }

    /**
     * Convert the set of ROI into a DetectionResult object containing a set of Detection.
     * 
     * @param rois
     *        ROI(s) to convert to Detection
     * @param sequence
     *        optional Sequence object allowing to feed some intensities information from Detection.
     * @throws InterruptedException
     */
    public static DetectionResult convertToDetectionResult(ROI[] rois, Sequence sequence) throws InterruptedException
    {
        final DetectionResult detectionResult = new DetectionResult();
        // create intensity descriptor
        final ROIIntensityDescriptorsPlugin intensityDescriptor = new ROIIntensityDescriptorsPlugin();

        for (ROI roi : rois)
        {
            final Point5D position = roi.getPosition5D();
            final Rectangle5D bounds = roi.getBounds5D();
            final Point5D massCenter = ROIMassCenterDescriptorsPlugin.computeMassCenter(roi);
            final double minIntensity;
            final double meanIntensity;
            final double maxIntensity;

            if (sequence != null)
            {
                // always compute intensity on channel 0 when we have infinite C position
                if (bounds.isInfiniteC())
                {
                    position.setC(0);
                    roi.setPosition5D(position);
                }

                final Map<ROIDescriptor, Object> results = intensityDescriptor.compute(roi, sequence);

                // restore original C position in ROI
                if (bounds.isInfiniteC())
                {
                    position.setC(-1);
                    roi.setPosition5D(position);
                }

                minIntensity = ((Double) results.get(ROIIntensityDescriptorsPlugin.minIntensityDescriptor))
                        .doubleValue();
                meanIntensity = ((Double) results.get(ROIIntensityDescriptorsPlugin.meanIntensityDescriptor))
                        .doubleValue();
                maxIntensity = ((Double) results.get(ROIIntensityDescriptorsPlugin.maxIntensityDescriptor))
                        .doubleValue();
            }
            else
            {
                minIntensity = 0d;
                meanIntensity = 0d;
                maxIntensity = 0d;
            }

            final ArrayList<Point3D> points3d = new ArrayList<Point3D>();
            final int minZ, maxZ;
            final int t, c;

            // infinite Z on ROI ?
            if (bounds.isInfiniteZ())
            {
                // limit Z to sequence Z size
                minZ = 0;
                maxZ = (sequence != null) ? sequence.getSizeZ() - 1 : 0;
            }
            else
            {
                minZ = (int) bounds.getMinZ();
                maxZ = (int) bounds.getMaxZ();
            }
            // infinite T on ROI ?
            if (bounds.isInfiniteT())
                t = 0;
            else
                t = (int) bounds.getT();
            // infinite C on ROI ?
            if (bounds.isInfiniteC())
                c = 0;
            else
                c = (int) bounds.getC();

            for (int z = minZ; z <= maxZ; z++)
            {
                final BooleanMask2D mask = roi.getBooleanMask2D(z, t, c, true);
                final Point[] pts2d = mask.getPoints();

                for (Point pt2d : pts2d)
                    points3d.add(new Point3D(pt2d.x, pt2d.y, z));
            }

            // create the spot
            final Spot spot = new Spot(massCenter.getX(), massCenter.getY(), massCenter.getZ(), minIntensity,
                    meanIntensity, maxIntensity, points3d);

            // and add it to detection result
            detectionResult.addDetection(t, spot);
        }

        detectionResult.setSequence(sequence);
        detectionResult.setName("Converted from " + rois.length + " ROI(s)");

        return detectionResult;
    }
}
